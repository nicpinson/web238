// Get value in search bar
$("#search-btn").click(function() {
	searchParameter = $("#searchParameter").val();
    const apiKey = "BAtZZd9EceUwtDlP9jjdaYbv5e94hxUA";
    const gifLimit = 8;

	fetchGifs(searchParameter, apiKey, gifLimit);
});

// Fetch data based on search parameters, return gifs to page
function fetchGifs(query, key, limit) {
	fetch(`https://api.giphy.com/v1/gifs/search?q=${query}&api_key=${key}&limit=${limit}`)
		.then((response) => response.json())
		.then((data) => {
        
            //console.log(data);
			const gifList = data.data.map((gifs) => {
				return `<img src="${gifs.images.original.url}" height="200" alt="${gifs.title}" class="gifThumbnails"/>`;
			}).join("");
			//console.log(gifList);
			$("#app").html(gifList);
		});
}