let updateScore = 0;

// When clicking on "Go To Questions" reset the score
$('#resetCounter').on("click", function() {
    updateScore = 0;
})

let isQuestion1Checked = false;
let isQuestion2Checked = false;
let isQuestion3Checked = false;
let isQuestion4Checked = false;

// Update bool to true if given radio set has been answered
$('.radioSet1').click(function() {
   if($('.radioSet1').is(':checked')) { isQuestion1Checked = true;}
});

$('.radioSet2').click(function() {
   if($('.radioSet2').is(':checked')) { isQuestion2Checked = true;}
});

$('.radioSet3').click(function() {
   if($('.radioSet3').is(':checked')) { isQuestion3Checked = true;}
});

$('.radioSet4').click(function() {
   if($('.radioSet4').is(':checked')) { isQuestion4Checked = true;}
});

// Check if all questions are answered when going back to Main Page. Enable the Result page link if TRUE.
$(".backToMain").on("click", function() {
    console.log("going back to main page");
    if (isQuestion1Checked && isQuestion2Checked && 
    isQuestion3Checked && isQuestion4Checked) {
        $("#resultsPage").removeClass("ui-state-disabled");
    }
})

// Check if correct input selected an increment score.
$(".backToMain").on("click", function() {
    if ($('input[name="question1"]:checked').val() == "q1correct1") {
        updateScore += 1;   
    }
    if ($('input[name="question2"]:checked').val() == "q2correct1") {
        updateScore += 1;  
    }
    if ($('input[name="question3"]:checked').val() == "q3correct1") {
        updateScore += 1;   
    }
    if ($('input[name="question4"]:checked').val() == "q4correct1") {
        updateScore += 1;   
    }
    // Update value on the Results Page
    $("#numOfCorrectAnswers").text(updateScore);
})
